boynton = [
    [-4, +2, -8],
    [-3, +5, +5],
    [-4, -4, +4],
    [+4, +12, 0],
    [+1, +9, -7],
    [-6, -4, -2],
    [+3, -1, -5],
    [-5, +3, -3]
]

# colors = [ # boynton 1989
#     '#952c3d', # [-4, +2, -8] red
#     '#358230', # [-3, +5, +5] green
#     '#186383', # [-4, -4, +4] blue
#     '#edd115', # [+4, +12, 0] yellow
#     '#e17624', # [+1, +9, -7] orange
#     '#4c3063', # [-6, -4, -2] purple
#     '#e4a4bf', # [+3, -1, -5] pink
#     '#6e442e', # [-5, +3, -3] brown
# ]

colors = ['#9b313a', '#357e2f', '#006382', '#facd00', '#ee7812', '#493463', '#e9a6bf', '#74442b']

if __name__ == '__main__':
    import numpy as np
    import matplotlib.pyplot as plt
    import colour
    import colour.plotting
    colors = []
    for ljg in boynton:
        xyz = colour.OSA_UCS_to_XYZ(ljg)
        rgb = colour.XYZ_to_sRGB(xyz / 100)
        rgb = np.clip(rgb, 0, 1)
        colors.append(colour.notation.RGB_to_HEX(rgb))
    print(colors)
    xys = []
    for ljg in boynton:
        xyz = colour.OSA_UCS_to_XYZ(ljg)
        xy = colour.XYZ_to_xy(xyz)
        rgb = colour.XYZ_to_sRGB(xyz / 100)
        rgb = np.clip(rgb, 0, 1)
        xyz_clipped = colour.sRGB_to_XYZ(rgb)
        xy_clipped = colour.XYZ_to_xy(xyz_clipped)
        xys.append(xy)
        xys.append(xy_clipped)
    colour.plotting.plot_chromaticity_diagram_CIE1931(standalone=False)
    for xy in xys:
        plt.plot(xy[0], xy[1], '.k')
    colour.plotting.render()
