import numpy as np
from psychopy import core, event, monitors, visual
from pyglet import gl as GL
from experiment import TrialType
import colors
import generator
import strings

SPEED = 30

class DotsStim:
    def __init__(self, win, edges=32, color=None):
        self.win = win
        self.edges = edges
        self.scrWidthCm = win.monitor.getWidth()
        self.scrWidthPix = win.monitor.getSizePix()[0]
        self.color = (np.array(color) + 1) / 2
        angles = np.arange(self.edges, dtype=np.float32) * 2 * np.pi / self.edges
        circleEdges = np.array(np.vstack((np.cos(angles), np.sin(angles))).T)
        triangles = np.zeros((self.edges, 3, 2), np.float32)
        triangles[:, 1] = circleEdges
        triangles[:, 2] = np.roll(circleEdges, -1, axis=0)
        self.circleTriangles = triangles.reshape((self.edges * 3, 2))

    def setCircles(self, circles):
        circles = np.array(circles, np.float32)
        circles = circles * self.scrWidthPix / self.scrWidthCm
        triangles = np.tile(self.circleTriangles, (circles.shape[0], 1))
        circles = np.repeat(circles, self.circleTriangles.shape[0], axis=0)
        radius = np.vstack((circles[:, 2], circles[:, 2])).T
        self.vertices = triangles * radius + circles[:, :2]

    def draw(self):
        self.win._setCurrent()
        GL.glPushMatrix()
        self.win.setScale('pix')

        # load Null textures into multitexteureARB - or they modulate glColor
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glEnable(GL.GL_TEXTURE_2D)
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
        GL.glActiveTexture(GL.GL_TEXTURE1)
        GL.glEnable(GL.GL_TEXTURE_2D)
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)

        GL.glEnable(GL.GL_LINE_SMOOTH)
        GL.glEnable(GL.GL_MULTISAMPLE)

        GL.glEnableClientState(GL.GL_VERTEX_ARRAY)
        GL.glColor3f(self.color[0], self.color[1], self.color[2])
        GL.glVertexPointer(2, GL.GL_FLOAT, 0, self.vertices.ctypes)
        GL.glDrawArrays(GL.GL_TRIANGLES, 0, self.vertices.shape[0])
        GL.glDisableClientState(GL.GL_VERTEX_ARRAY)

        GL.glPopMatrix()

class Circles:
    def __init__(self, win):
        self.win = win
        self.dots = DotsStim(win, color=win.default_color)
        self.number = 0
        self.tutorial = visual.TextStim(win, font=strings.font)

    def update(self, trial, seed):
        q = trial['quantity']
        self.number = int(np.round(2 ** (q/3) * 15, 0))
        circles = generator.generate(self.number, seed)
        self.dots.setCircles(circles)
        self.tutorial.text = strings.more_dots if q else strings.less_dots

    def loop(self, trial_type):
        for _ in range(SPEED):
            self.dots.draw()
            if trial_type == TrialType.TRAINING:
                self.tutorial.draw()
            self.win.flip()

class BigCircle:
    def __init__(self, win):
        self.win = win
        self.circle = visual.Circle(win, lineColor=win.default_color, edges=128,
                                    lineWidth=.2 * win.prefs.resolution[0] / win.prefs.width)
        self.tutorial = visual.TextStim(win, font=strings.font)

    def update(self, trial, seed):
        q = trial['quantity']
        radius = np.round(2 ** (q/3) * 2, 2)
        self.circle.radius = radius
        random_state = np.random.RandomState(seed)
        pos_r = np.min((radius, 8 - radius)) * np.sqrt(random_state.random_sample())
        pos_t = 2 * np.pi * random_state.random_sample()
        self.circle.pos = [pos_r * np.cos(pos_t), pos_r * np.sin(pos_t)]
        self.tutorial.text = strings.big_circle if q else strings.small_circle

    def loop(self, trial_type):
        for _ in range(SPEED):
            self.circle.draw()
            if trial_type == TrialType.TRAINING:
                self.tutorial.draw()
            self.win.flip()

class KeyStim:
    def __init__(self, win):
        self.win = win
        self.tutorial = visual.TextStim(win, font=strings.font,
                                        color=win.default_color,
                                        pos=[0, -6],
                                        wrapWidth=30)

    def update(self, trial_type, magnitude):
        self.tutorial.text = (strings.keystim_testing if trial_type == TrialType.TESTING
                              else strings.keystim_validation).format(
            *((strings.small_circle, strings.big_circle) if magnitude
              else (strings.less_dots, strings.more_dots)))

    def draw(self):
        self.tutorial.draw()
        self.win.flip()

class ColorCognitiveLoad:
    def __init__(self, win):
        self.win = win
        self.tutorial_begin = visual.TextStim(win, strings.cogload_begin, strings.font,
                                              color=win.default_color,
                                              pos=[0, -6])
        self.tutorial_end = visual.TextStim(win, strings.cogload_end, strings.font,
                                            color=win.default_color,
                                            pos=[0, -6])
        self.color_stims = [visual.Circle(win,
                                          pos=[2.5 * i, 2.5 * j],
                                          radius=2,
                                          edges=64,
                                          lineColor=None)
                            for i in [-1, 1] for j in [-1, 1]]
        self.asked_color_stim = visual.Circle(win,
                                              pos=[0, 0],
                                              radius=2,
                                              edges=64,
                                              lineColor=None)
        self.choosed_colors = []
        self.asked_color = 0

    def update(self, trial_type):
        self.trial_type = trial_type
        self.choosed_colors = np.random.choice(len(colors.colors), 4, replace=False)
        for stim, color in zip(self.color_stims, self.choosed_colors):
            stim.fillColor = colors.colors[color]
        self.asked_color = np.random.choice(len(colors.colors))
        self.asked_color_stim.fillColor = colors.colors[self.asked_color]
        self.is_shown = self.asked_color in self.choosed_colors

    def begin(self):
        for _ in range(SPEED):
            if self.trial_type == TrialType.VALIDATION:
                self.tutorial_begin.draw()
            for stim in self.color_stims:
                stim.draw()
            self.win.flip()

    def end(self):
        self.tutorial_end.draw()
        self.asked_color_stim.draw()
        self.win.flip()

class Attention:
    def __init__(self, win):
        self.win = win
        self.lines = [visual.Line(win, (0, -.25), (0, .25), lineColor=win.default_color),
                      visual.Line(win, (-.25, 0), (.25, 0), lineColor=win.default_color)]

    def loop(self, handler, trial):
        delay = 0
        if handler.type == TrialType.TESTING and 7 * handler.thisRepN + handler.thisTrialN in handler.breaks:
            delay = SPEED * (5 if trial['cognitive_load'] else 3)
        for _ in range(SPEED + delay):
            for line in self.lines:
                line.draw()
            self.win.flip()

class Feedback:
    def __init__(self, win, text, color, height):
        self.win = win
        self.text = visual.TextStim(win, text, strings.font,
                                    color=color, height=height)

    def loop(self):
        for _ in range(SPEED):
            self.text.draw()
            self.win.flip()

class Wrong(Feedback):
    def __init__(self, win):
        super().__init__(win, strings.ic_wrong, colors.colors[0], 16)

class Correct(Feedback):
    def __init__(self, win):
        super().__init__(win, strings.ic_correct, colors.colors[1], 10)

class Render:
    def __init__(self, win):
        self.win = win
        self.circles = Circles(win)
        self.bigcircle = BigCircle(win)
        self.load = ColorCognitiveLoad(win)
        self.keystim = KeyStim(win)
        self.attention = Attention(win)
        self.wrong = Wrong(win)
        self.correct = Correct(win)

    def loop(self, trial, handler):
        seed = np.random.randint(0, np.iinfo(np.uint32).max, dtype=np.uint32)
        if handler.type == TrialType.TESTING:
            handler.addData('seed', seed)

        self.load.update(handler.type)
        self.keystim.update(handler.type, trial['magnitude'])
        self.bigcircle.update(trial, seed)
        self.circles.update(trial, seed)

        if not self.win.prefs.debug:
            self.attention.loop(handler, trial)

        if trial['cognitive_load']:
            self.load.begin()

        # 0 is number, 1 is space
        if trial['magnitude']:
            self.bigcircle.loop(handler.type)
        else:
            self.circles.loop(handler.type)

        if handler.type != TrialType.TRAINING:
            self.keystim.draw()
            self.win.clock.reset()
            keys = event.waitKeys(keyList=['left', 'right'], timeStamped=self.win.clock)
            is_true = (keys[0][0] == 'right') == (trial['quantity'] == 6)
            if handler.type == TrialType.VALIDATION:
                if is_true:
                    self.correct.loop()
                else:
                    self.wrong.loop()
                    handler.lives -= 1
            else:
                handler.addData('right_key', keys[0][0] == 'right')
                handler.addData('rt', keys[0][1])

        if trial['cognitive_load']:
            self.load.end()
            self.win.clock.reset()
            keys = event.waitKeys(keyList=['left', 'right'], timeStamped=self.win.clock)
            is_true = (keys[0][0] == 'right') == self.load.is_shown
            if handler.type == TrialType.VALIDATION:
                if is_true:
                    self.correct.loop()
                else:
                    self.wrong.loop()
                    handler.lives -= 1
            else:
                handler.addData('cogload_colors', 'i' + ''.join(map(str, self.load.choosed_colors)))
                handler.addData('cogload_asked', self.load.asked_color)
                handler.addData('cogload_right', is_true)
                handler.addData('cogload_rt', keys[0][1])
        else:
            if handler.type == TrialType.TESTING:
                handler.addData('cogload_colors', 'i0000')
                handler.addData('cogload_asked', 0)
                handler.addData('cogload_right', False)
                handler.addData('cogload_rt', 0)

def create(prefs):
    max_samples = GL.GLint()
    GL.glGetIntegerv(GL.GL_MAX_SAMPLES, max_samples)
    monitor = monitors.Monitor('')
    monitor.setWidth(prefs.width)
    monitor.setSizePix(prefs.resolution)
    win = visual.Window(size=prefs.resolution,
                        fullscr=prefs.full_screen,
                        allowGUI=not prefs.full_screen,
                        monitor=monitor,
                        units='cm',
                        multiSample=True,
                        numSamples=max_samples.value)
    win.clock = core.Clock()
    win.default_color = (-1, -1, -1)
    win.prefs = prefs
    return win

def main():
    import sys, time
    from timeit import timeit
    from types import SimpleNamespace
    if sys.platform == 'win32':
        prefs = SimpleNamespace(width=34.54, resolution=(1366, 768), full_screen=False)
    elif sys.platform == 'darwin':
        prefs = SimpleNamespace(width=28.65, resolution=(1440, 900), full_screen=False)
    win = create(prefs)
    circles = Circles(win)
    def bench(graphics=False, random=True):
        seed = np.random.randint(0, np.iinfo(np.uint32).max, dtype=np.uint32)
        quantity = np.random.choice([0, 6]) if random else 6
        print('Begin', time.time())
        circles.update({'quantity': quantity}, seed)
        print('End', time.time())
        if graphics:
            circles.loop(1)
    bench()
    print(timeit(lambda: bench(False), number=100) / 100)
    print(timeit(lambda: bench(True), number=10) / 10)

if __name__ == '__main__':
    main()
