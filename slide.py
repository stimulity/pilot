import numpy as np
from psychopy import event, visual
from psychopy.visual.line import Line
from psychopy.visual.bufferimage import BufferImageStim
from experiment import TrialType
import strings

LIGHT_GRAY = '#afafaf'
GRAY = '#545454'
BLACK = '#000000'

class Progress:
    def __init__(self, win, handlers):
        self.win = win
        self.handlers = handlers
        self.layout = [[visual.TextStim(win, font=strings.font, height=.1,
                                        color=win.default_color, units='norm',
                                        pos=[-.9, .6 + -(i+i//5) * .14],
                                        text=strings.ic_circle_white,
                                        alignHoriz='left'),
                        visual.TextStim(win, font=strings.font, height=.05,
                                        color=win.default_color, units='norm',
                                        pos=[-.85, .6 + -(i+i//5) * .14],
                                        text=handlers[i].title,
                                        alignHoriz='left')]
                       for i in range(len(handlers))]
        self.headers = [visual.TextStim(win, font=strings.font + ' Black',
                                        height=.04, units='norm',
                                        pos=[-.9, i * .85 - .15],
                                        color=GRAY,
                                        alignHoriz='left',
                                        text=(strings.level_dots if handlers.first_space ^ i
                                              else strings.level_circle))
                        for i in range(2)]
        self.line = Line(win, (-.55, -.8), (-.55, .8), units='norm', lineColor=GRAY)

    def update(self, handler):
        index = self.handlers.index(handler)
        for i, item in enumerate(self.layout):
            if i < index:
                item[0].text = strings.ic_circle_black
                item[0].color = GRAY
                item[1].color = GRAY
            elif i == index:
                item[0].text = strings.ic_circle_radio
                item[0].color = BLACK
                item[1].color = BLACK
            else:
                item[0].text = strings.ic_circle_white
                item[0].color = GRAY
                item[1].color = GRAY

        self.buffer = BufferImageStim(self.win, stim=(
            [item for row in self.layout for item in row]
            + self.headers + [self.line]))

    def draw(self):
        self.buffer.draw()

class Timer:
    def __init__(self, win):
        self.win = win
        self.background = Line(win, (-1, 1), (1, 1), units='norm',
                               lineWidth=win.prefs.resolution[1] * .02, lineColor=GRAY)
        self.foreground = Line(win, (-1, 1), (1, 1), units='norm',
                               lineWidth=win.prefs.resolution[1] * .02, lineColor=LIGHT_GRAY)

    def update(self, t, d):
        self.foreground.end = (np.log2(t / d + 1) * 2 - 1, 1)

    def draw(self):
        self.background.draw()
        self.foreground.draw()

class Slides:
    def __init__(self, win, handlers):
        self.win = win
        self.title = visual.TextStim(win, font=strings.font,
                                     color=win.default_color,
                                     wrapWidth=30)
        self.tutorial = visual.TextStim(win, font=strings.font,
                                        color=win.default_color, units='norm',
                                        pos=[-.5, 0], wrapWidth=30,
                                        alignHoriz='left')
        self.timer = Timer(win)
        self.progress = Progress(win, handlers)

    def show_text_layout(self, text):
        self.title.text = text
        for _ in range(60):
            self.title.draw()
            self.win.flip()
        event.waitKeys()
        self.win.flip()

    def show_tutorial(self, handler):
        self.progress.update(handler)
        self.tutorial.text = handler.description
        time = 15
        for t in range(time * 60):
            self.timer.update(t, time * 60)
            self.progress.draw()
            self.timer.draw()
            self.tutorial.draw()
            self.win.flip()

    def show_initial(self):
        self.show_text_layout(strings.description)

    def show_end(self):
        self.show_text_layout(strings.end_description)
