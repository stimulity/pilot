import numpy as np

# Uncomment to use numba
#from numba import njit

SIZE_R = 8
CUMULATIVE_R = 4

# begin third-party
# d3-hierarchy/pack/siblings.js | Mike Bostock | BSD 3-Clause

#njit(fastmath=True)
def place(b, a, c):
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    d2 = dx * dx + dy * dy
    if d2 != 0:
        a2 = a[2] + c[2]
        a2 *= a2
        b2 = b[2] + c[2]
        b2 *= b2
        if a2 > b2:
            x = (d2 + b2 - a2) / (2 * d2)
            y2 = b2 / d2 - x * x
            if y2 > 0:
                y = y2 ** .5
            else:
                y = 0
            c[0] = b[0] - x * dx - y * dy
            c[1] = b[1] - x * dy + y * dx
        else:
            x = (d2 + a2 - b2) / (2 * d2)
            y2 = a2 / d2 - x * x
            if y2 > 0:
                y = y2 ** .5
            else:
                y = 0
            c[0] = a[0] + x * dx - y * dy
            c[1] = a[1] + x * dy + y * dx
    else:
        c[0] = a[0] + c[2]
        c[1] = a[1]

#njit(fastmath=True)
def intersects(a, b):
    dr = a[2] + b[2] - 1e-6
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    return dr > 0 and dr * dr > dx * dx + dy * dy

#njit(fastmath=True)
def score(node0, node10):
    a = node0
    b = node10
    ab = a[2] + b[2]
    dx = (a[0] * b[2] + b[0] * a[2]) / ab
    dy = (a[1] * b[2] + b[1] * a[2]) / ab
    return dx * dx + dy * dy

#njit(fastmath=True)
def packEnclose(circles, random_indices):
    n = len(circles)
    if n == 0:
        return 0

    # Place the first circle.
    value_a = circles[0]
    value_a[0] = 0
    value_a[1] = 0
    if n == 1:
        return value_a[2]

    # Place the second circle.
    value_b = circles[1]
    value_a[0] = -value_b[2]
    value_b[0] = value_a[2]
    value_b[1] = 0
    if n == 2:
        return value_a[2] + value_b[2]

    # Place the third circle.
    value_c = circles[2]
    place(value_b, value_a, value_c)

    # Mem [[self_index, next_index, previous_index]]
    mem = np.zeros((n, 3), np.intp)
    mem[0][0] = 0; mem[0][1] = 1; mem[0][2] = 2
    mem[1][0] = 1; mem[1][1] = 2; mem[1][2] = 0
    mem[2][0] = 2; mem[2][1] = 0; mem[2][2] = 1

    # Initialize the front-chain using the first three circles a, b and c.
    node_a = mem[0]
    node_b = mem[1]

    # Attempt to place each remaining circle...
    i = 3
    while i < n:
        value_c = circles[i]
        place(circles[node_a[0]], circles[node_b[0]], value_c)

        # Find the closest intersecting circle on the front-chain, if any.
        # "Closeness" is determined by linear distance along the front-chain.
        # "Ahead" or "behind" is likewise determined by linear distance.
        j = mem[node_b[1]]
        k = mem[node_a[2]]
        sj = circles[node_b[0]][2]
        sk = circles[node_a[0]][2]

        breaker = False
        while True:
            if sj <= sk:
                if intersects(circles[j[0]], value_c):
                    node_b = j
                    node_a[1] = node_b[0]
                    node_b[2] = node_a[0]
                    breaker = True
                    break
                sj += circles[j[0]][2]
                j = mem[j[1]]
            else:
                if intersects(circles[k[0]], value_c):
                    node_a = k
                    node_a[1] = node_b[0]
                    node_b[2] = node_a[0]
                    breaker = True
                    break
                sk += circles[k[0]][2]
                k = mem[k[2]]
            if j[0] == k[1]:
                break

        if breaker:
            continue

        # Success! Insert the new circle c between a and b.
        mem[i][0] = i; mem[i][1] = node_b[0]; mem[i][2] = node_a[0]
        node_c = mem[i]

        node_b[2] = i
        node_a[1] = i
        node_b = node_c

        # Compute the new closest circle pair to the centroid.
        aa = score(circles[node_a[0]], circles[mem[node_a[1]][0]])
        node_c = mem[node_c[1]]

        while node_c[0] != node_b[0]:
            ca = score(circles[node_c[0]], circles[mem[node_c[1]][0]])
            if ca < aa:
                node_a = node_c
                aa = ca
            node_c = mem[node_c[1]]
        node_b = mem[node_a[1]]
        i += 1

    # Compute the enclosing circle of the front chain.
    ex, ey, er = enclose(circles[random_indices])

    # Translate the circles to put the enclosing circle around the origin.
    circles[:, 0] -= ex
    circles[:, 1] -= ey

    return er

# d3-hierarchy/pack/enclose.js | Mike Bostock | BSD 3-Clause

#njit(fastmath=True)
def enclose(circles):
    n = len(circles)
    B = [np.zeros(3) for _ in range(0)]
    e = (0, 0, 0)
    i = 0

    while i < n:
        p = circles[i]
        if e[2] != 0 and enclosesWeak(e, p):
            i += 1
        else:
            B = extendBasis(B, p)
            e = encloseBasis(B)
            i = 0
    return e

#njit(fastmath=True)
def extendBasis(B, p):
    if enclosesWeakAll(p, B):
        return [p]

    # If we get here then B must have at least one element.
    for i in range(len(B)):
        if enclosesNot(p, B[i]) and enclosesWeakAll(encloseBasis2(B[i], p), B):
            return [B[i], p]

    # If we get here then B must have at least two elements.
    for i in range(len(B) - 1):
        for j in range(i + 1, len(B)):
            if (enclosesNot(encloseBasis2(B[i], B[j]), p) and
                enclosesNot(encloseBasis2(B[i], p), B[j]) and
                enclosesNot(encloseBasis2(B[j], p), B[i]) and
                enclosesWeakAll(encloseBasis3(B[i], B[j], p), B)):
                return [B[i], B[j], p]

    # If we get here then something is very wrong.
    raise RuntimeError

#njit(fastmath=True)
def enclosesNot(a, b):
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    dr = a[2] - b[2]
    return dr < 0 or dr * dr < dx * dx + dy * dy

#njit(fastmath=True)
def enclosesWeak(a, b):
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    dr = a[2] - b[2] + 1e-6
    return dr > 0 and dr * dr > dx * dx + dy * dy

#njit(fastmath=True)
def enclosesWeakAll(a, B):
    for i in range(len(B)):
        if not enclosesWeak(a, B[i]):
            return False
    return True

#njit(fastmath=True)
def encloseBasis(B):
    n = len(B)
    if n == 1:
        x, y, r = B[0]
        return (x, y, r)
    elif n == 2:
        return encloseBasis2(B[0], B[1])
    elif n == 3:
        return encloseBasis3(B[0], B[1], B[2])
    raise RuntimeError

#njit(fastmath=True)
def encloseBasis2(a, b):
    x1, y1, r1 = a
    x2, y2, r2 = b
    x21 = x2 - x1
    y21 = y2 - y1
    r21 = r2 - r1
    l = (x21 * x21 + y21 * y21) ** .5
    return ((x1 + x2 + x21 / l * r21) / 2,
            (y1 + y2 + y21 / l * r21) / 2,
            (l + r1 + r2) / 2)

#njit(fastmath=True)
def encloseBasis3(a, b, c):
    x1, y1, r1 = a
    x2, y2, r2 = b
    x3, y3, r3 = c
    a2 = x1 - x2
    a3 = x1 - x3
    b2 = y1 - y2
    b3 = y1 - y3
    c2 = r2 - r1
    c3 = r3 - r1
    d1 = x1 * x1 + y1 * y1 - r1 * r1
    d2 = d1 - x2 * x2 - y2 * y2 + r2 * r2
    d3 = d1 - x3 * x3 - y3 * y3 + r3 * r3
    ab = a3 * b2 - a2 * b3
    xa = (b2 * d3 - b3 * d2) / (ab * 2) - x1
    xb = (b3 * c2 - b2 * c3) / ab
    ya = (a3 * d2 - a2 * d3) / (ab * 2) - y1
    yb = (a2 * c3 - a3 * c2) / ab
    A = xb * xb + yb * yb - 1
    B = 2 * (r1 + xa * xb + ya * yb)
    C = xa * xa + ya * ya - r1 * r1
    r = -(B + (B * B - 4 * A * C) ** .5) / (2 * A) if A != 0 else -C / B
    return (x1 + xa + xb * r, y1 + ya + yb * r, r)

# end

def generate(n, seed):
    random_state = np.random.RandomState(seed)
    data = random_state.random_sample(n)
    data = 2 ** (data - .5)
    circles = np.zeros((n, 3))
    circles[:, 2] = data
    random_indices = random_state.permutation(n)
    er = packEnclose(circles, random_indices)
    circles *= SIZE_R / er
    r = CUMULATIVE_R / np.sqrt(np.dot(circles[:, 2], circles[:, 2]))
    return np.hstack((circles[:, :2], circles[:, 2:] * r))

def main():
    for i in range(1):
        for q in range(7):
            n = int(np.round(2 ** (q/3) * 15, 0))
            seed = np.random.randint(0, np.iinfo(np.uint32).max, dtype=np.uint32)
            generate(n, seed)

if __name__ == '__main__':
    main()
