font = 'Source Sans Pro'

ic_left = '\u25c0'
ic_right = '\u25b6'
ic_wrong = '\u00d7'
ic_correct = '\u2713'
ic_circle_black = '\u25cf'
ic_circle_white = '\u25cb'
ic_circle_radio = '\u25c9'

saving = 'Kaydediliyor...'

failed_handedness = 'Solak olduğunuz için deneye katılamazsınız.'
failed_colorblind = 'Renk körü olduğunuz için deneye katılamazsınız.'
failed_form = 'Formdaki ölçütlere uygun olmadığınız için deneye katılamazsınız.'

colorblind = 'Resimdeki sayıyı yazan seçeneğe tıklayın. Eğer sayı yoksa, Yok\'a tıklayın.'

preferences = {
    'Geliştirici Modu': ['Kapalı', 'Açık'],
    'Ekran Boyutu (inç)': [
        '13.3', '10.1', '11.6', '12.0', '12.5', '12.9', '13.5', '14.1', '15.4',
        '15.6', '17.0', '17.3', '18.5', '19.0', '19.5', '20.0', '20.7', '21.5',
        '22.0', '23.0', '23.5', '23.6', '23.8', '24.0', '24.1', '24.5', '25.0',
        '27.0', '28.0', '29.0', '30.0', '31.0', '31.5', '32.0', '34.0', '35.0',
        '37.5', '38.0', '42.0', '43.0', '48.0', '49.0', '55.0'],
    'Ekran Çözünürlüğü': [
        '1440x900', '240x160', '480x320', '640x320', '1024x576', '1024x600',
        '1024x640', '1024x768', '1136x640', '1152x720', '1152x768', '1280x720',
        '1280x768', '1280x800', '1280x854', '1334x750', '1366x768', '1440x960',
        '1440x1024', '1440x1080', '1600x768', '1600x900', '1600x1024',
        '1680x1050', '1776x1000', '1920x1080', '1920x1200', '1920x1280',
        '1920x1400', '2048x1080', '2048x1152', '2048x1280', '2160x1080',
        '2160x1200', '2160x1440', '2256x1504', '2280x1080', '2304x1440',
        '2436x1125', '2538x1080', '2560x1080', '2560x1440', '2560x1600',
        '2560x1700', '2560x1800', '2736x1824', '2880x900', '2880x1440',
        '2880x1620', '2880x1800', '2960x1440', '3000x2000', '3200x1800',
        '3200x2048', '3240x2160', '3440x1440', '3840x1600', '3840x2160',
        '3840x2400', '4096x2160', '4500x3000', '5120x2160', '5120x2880',
        '5120x3200', '5120x4096', '6400x4096', '7680x4320', '7680x4800',
        '8192x4320', '8192x4608'],
    'Tam Ekran': ['Açık', 'Kapalı']
}

form_data_1 = {
    'age': ['18', '19', '20', '21', '22', '23', '24', '25',
            '18 yaş altı veya 25 yaş üstü'],
    'gender': ['Erkek', 'Kadın'],
    'language': ['Türkçe', 'İngilizce', 'Fransızca', 'Almanca',
                 'İspanyolca', 'Arapça', 'Farsça', 'İbranice',
                 'Diğer (Yazı sistemi soldan sağa olan)',
                 'Diğer (Yazı sistemi sağdan sola olan)'],
    'requirement0': ['Evet', 'Hayır'],
    'requirement1': ['Hayır', 'Evet'],
    'requirement2': ['Hayır', 'Evet']
}

invalid_age = '18 yaş altı veya 25 yaş üstü'
invalid_requirement = ['Hayır', 'Evet', 'Evet']
invalid_languages = ['Arapça', 'Farsça', 'İbranice',
                     'Diğer (Yazı sistemi sağdan sola olan)']

form_data_2 = {
    'hand_writing': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_drawing': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_throwing': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_hammer': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_brushing': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_erasing': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_scissor': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_match': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_mixing': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_spoon': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_screwdriver': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_jar': ['Sağ', 'Sol', 'Her ikisi de'],
    'hand_knife': ['Sağ', 'Sol', 'Her ikisi de']
}

form_label_1 = {
    'age': 'Yaşınız?',
    'gender': 'Cinsiyetiniz?',
    'language': 'Anadiliniz?',
    'requirement0': 'Normal veya düzeltilmiş bir görüşe sahip misiniz?',
    'requirement1': 'Herhangi bir nörolojik veya psikiyatrik rahatsızlığınız var mı?',
    'requirement2': 'Sinir sistemine etkili herhangi bir ilaç kullanıyor musunuz?'
}

form_label_2 = {
    '__text__0': 'El tercihi',
    '__text__1': 'Aşağıda belirtilen işleri yaparken öncelikle tercih ettiğiniz elinizi işaretleyiniz.',
    '__text__2': 'İki elinizden herhangi birini öncelikle tercih etmiyorsanız "Her ikisi de" yanıtını işaretleyiniz.',
    'hand_writing': 'Yazı yazarken',
    'hand_drawing': 'Çizerken',
    'hand_throwing': 'Bir şey fırlatırken',
    'hand_hammer': 'Çekiç kullanırken (çekici tutan el)',
    'hand_brushing': 'Diş fırçalarken',
    'hand_erasing': 'Silgi ile silerken',
    'hand_scissor': 'Makas kullanırken',
    'hand_match': 'Kibrit çakarken',
    'hand_mixing': 'Bir teneke boya karıştırırken',
    'hand_spoon': 'Kaşık kullanırken',
    'hand_screwdriver': 'Tornavida kullanırken',
    'hand_jar': 'Kavanoz kapağı açarken (kapağı açan el)',
    'hand_knife': 'Bıçak kullanırken (çatalsız)'
}

none_label = 'Yok'

# np.random.seed(2); np.random.randint(100, size=(54,))
colorblind_labels = [
    ['12', none_label, '40', '15', '72', '22'],
    ['8', '3', none_label, '43', '82', '75'],
    ['29', '70', none_label, '7', '34', '49'],
    ['5', '2', none_label, '95', '75', '85'],
    ['3', '5', none_label, '47', '63', '31'],
    ['15', '17', none_label, '90', '20', '37'],
    ['74', '21', none_label, '39', '67', '4'],
    ['6', none_label, '42', '51', '38', '33'],
    ['45', none_label, '58', '67', '69', '88'],
    ['5', none_label, '68', '46', '70', '95'],
    ['7', none_label, '83', '31', '66', '80'],
    ['16', none_label, '52', '76', '50', '4'],
    ['73', none_label, '90', '63', '79', '49'],
    [none_label, '5', '39', '46', '8', '50'],
    [none_label, '45', '15', '8', '17', '22'],
]

cogload_begin = 'Renkleri hatırlayın.'
cogload_end = f'Bu renk gösterildi mi?\n Hayır ({ic_left})        Evet ({ic_right})'

keystim_choices = f'{{}} ({ic_left})        {{}} ({ic_right})'
keystim_validation = 'Gösterilen büyüklük hangisidir?\n' + keystim_choices
keystim_testing = 'Gösterilen büyüklük hangisine yakındır?\n' + keystim_choices

less_dots = 'Az nokta'
more_dots = 'Çok nokta'
small_circle = 'Küçük çember'
big_circle = 'Büyük çember'

description = 'Devam etmek için bir tuşa basın.'

level_dots = 'NOKTALAR  BÖLÜMÜ'
level_circle = 'ÇEMBER  BÖLÜMÜ'
phase_training = 'Alıştırma'
phase_validation = 'Doğrulama'
phase_testing = 'Test'
block_1 = 'Temel'
block_2 = 'Renk'

training = (f'Bu aşamada standart büyüklükleri öğreneceksiniz.\n\n'
            f'Ekranda {{}} ve {{}}\n'
            f'büyüklükleri gösterilecek.\n\n'
            f'Bu standart büyüklükleri aklınızda tutarak\n'
            f'gelecek aşamalardaki soruları cevaplayacaksınız.')
validation = (f'Bu aşamada göreceğiniz büyüklüklerin\n'
              f'aklınızda tuttuğunuz standart büyüklüklerden\n'
              f'hangisi olduğuna cevap vereceksiniz.\n\n'
              f'Gördüğünüz büyüklük;\n'
              f'{{}} ise {ic_left},\n'
              f'{{}} ise {ic_right}\n'
              f'tuşuna basın.\n\n'
              f'Olabildiğince doğru ve hızlı yanıt vermelisiniz.')
validation_cl = (f'Bu aşamada renk kısmı bulunuyor.\n\n'
                 f'Ekranda önce renkler, sonra büyüklükler,\n'
                 f'ardından renklerle ilgili bir soru gösterilecek.\n\n'
                 f'Önce gösterilen renkleri hatırlayın.\n'
                 f'Sonra büyüklüğü önceki aşamadaki gibi cevaplayın.\n'
                 f'Ardından hatırlanacak renkler ile soruyu cevaplayın.')
testing = (f'Bu aşamada standart büyüklüklerle birlikte\n'
           f'ara büyüklükler de göreceksiniz.\n\n'
           f'Gördüğünüz büyüklük;\n'
           f'{{}} YAKIN ise {ic_left},\n'
           f'{{}} YAKIN ise {ic_right} tuşuna basın.\n\n'
           f'{ic_correct} veya {ic_wrong} gösterilmeyecek.')
testing_cl = (f'Bu aşamada hem renk kısmını yapacaksınız\n'
              f'hem de standart büyüklüklerle birlikte\n'
              f'ara büyüklükler göreceksiniz.\n\n'
              f'Gördüğünüz büyüklük;\n'
              f'{{}} YAKIN ise {ic_left},\n'
              f'{{}} YAKIN ise {ic_right} tuşuna basın.\n\n'
              f'{ic_correct} veya {ic_wrong} gösterilmeyecek.')

end_description = 'Deneyimize katıldığınız için teşekkür ederiz.'

def get_title(phase, block=None):
    if phase == 0:
        return phase_training
    elif phase == 1:
        return ': '.join((phase_validation, block_2 if block else block_1))
    else:
        return ': '.join((phase_testing, block_2 if block else block_1))

def get_description(level, phase, block=None):
    level_format = ((small_circle.lower(), big_circle.lower()) if level
                    else (less_dots.lower(), more_dots.lower()))
    if phase == 0:
        return training.format(*level_format)
    elif phase == 1:
        if block == 0:
            return validation.format(*level_format)
        else:
            return validation_cl.format(*level_format)
    else:
        level_format = map(lambda s: s + ('e' if level else 'ya'), level_format)
        if block == 0:
            return testing.format(*level_format)
        else:
            return testing_cl.format(*level_format)
