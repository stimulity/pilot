import os
import numpy as np
from psychopy import gui
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5.QtCore import Qt
import strings

PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'colorblind')
PDF_PATH = os.path.join(PATH, 'ishihara.pdf')
MAIN_SET = [3, 7, 9, 13, 15]
EXTRA_SET = [2, 4, 5, 6, 8, 10, 11, 12, 14]

def download():
    import urllib.request
    URL = 'http://www.dfis.ubi.pt/~hgil/P.V.2/Ishihara/Ishihara.24.Plate.TEST.Book.pdf'
    print('Colorblindness plates not found')
    urllib.request.urlretrieve(URL, PDF_PATH, lambda c, s, t: print('Downloading... {}%'.format(int(np.round(c * s / t * 100))), end='\r'))
    print()
    print('Download complete')

def extract():
    '''nedbatchelder.com/blog/200712/extracting_jpgs_from_pdfs.html | CC BY-NC-SA 1.0'''
    with open(PDF_PATH, 'rb') as f:
        pdf = f.read()
    startmark = b'\xff\xd8'
    endmark = b'\xff\xd9'
    i = 0
    n = 0
    while True:
        istream = pdf.find(b'stream', i)
        if istream < 0:
            break
        istart = pdf.find(startmark, istream, istream+20)
        if istart < 0:
            i = istream+20
            continue
        i = pdf.find(b'endstream', istart)
        i = pdf.find(endmark, i-20)
        i += 2
        n += 1
        if 8 <= n <= 22:
            name = str(n - 7) + '.jpg'
            with open(os.path.join(PATH, name), 'wb') as w:
                w.write(pdf[istart:i])

def ensure_files():
    if not os.path.isdir(PATH):
        os.makedirs(PATH)

    files_exist = True
    for i in range(1, 16):
        if not os.path.isfile(os.path.join(PATH, f'{i}.jpg')):
            files_exist = False

    if not files_exist:
        download()
        extract()
        os.remove(PDF_PATH)

def create_dialog(i):
    labels = strings.colorblind_labels[i-1]
    correct_label = labels[0]
    labels = sorted(labels, key=lambda s: int(s) if s.isdigit() else -1)
    dialog = gui.Dlg('Renk Körlüğü Testi')
    dialog.setWindowFlags(Qt.WindowStaysOnTopHint)
    pixmap = QtGui.QPixmap(os.path.join(PATH, f'{i}.jpg'))
    pixmap = pixmap.scaledToWidth(512, Qt.SmoothTransformation)
    plate = QtWidgets.QLabel(dialog)
    plate.setPixmap(pixmap)
    dialog.layout.addWidget(plate, dialog.irow, 0, 1, 2)
    dialog.irow += 1
    dialog.addText(strings.colorblind)
    dialog.buttonBox.clear()
    for label in labels:
        button = QtWidgets.QPushButton(label, dialog)
        dialog.buttonBox.addButton(button, QtWidgets.QDialogButtonBox.ActionRole)
        button.clicked.connect(dialog.accept if label == correct_label else dialog.reject)
    return dialog

def show_dialog(logger):
    score = 0
    for i in np.concatenate(([1], np.random.permutation(MAIN_SET))):
        dialog = create_dialog(i)
        dialog.show()
        if dialog.OK:
            score += 1
    if score == 6:
        logger.info('Colorblind|%s', {'extra': False, 'score': score})
        return True
    for i in np.random.permutation(EXTRA_SET):
        dialog = create_dialog(i)
        dialog.show()
        if dialog.OK:
            score += 1
    logger.info('Colorblind|%s', {'extra': True, 'score': score})
    return score >= 13
