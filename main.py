import logging, os, sys, time
import colorblind, dialog, display, experiment, slide, strings

class App:
    def __init__(self, prefs):
        self.create_files()
        self.logger.info('Preferences|%s', prefs.__dict__)
        if not prefs.debug:
            dialog.show_all(prefs, self.logger)
        self.win = display.create(prefs)
        self.handlers = experiment.HandlerList(self.path, self.logger)
        self.render = display.Render(self.win)
        self.slides = slide.Slides(self.win, self.handlers)

    def create_files(self):
        folder = os.path.join(os.path.expanduser('~'), 'Experiments')
        if not os.path.isdir(folder):
            os.makedirs(folder)

        name = str(int(time.time()))
        self.path = os.path.join(folder, name)

        self.logger = logging.getLogger('pilot')
        self.logger.setLevel(logging.INFO)
        fh = logging.FileHandler(self.path + '.journal', encoding='utf-8')
        fh.setLevel(logging.INFO)
        fh.setFormatter(logging.Formatter('%(asctime)s|%(message)s'))
        self.logger.addHandler(fh)

    def loop(self, handler):
        if handler.type == experiment.TrialType.VALIDATION:
            handler = handler.new()
        for trial in handler:
            self.render.loop(trial, handler)
            if handler.type == experiment.TrialType.VALIDATION and handler.lives < 0:
                return True
        if handler.type == experiment.TrialType.TESTING:
            self.handlers.save(handler)
        return False

    def run(self):
        self.logger.info('Main|Init')
        self.slides.show_initial()
        self.logger.info('Main|Begin')
        for handler in self.handlers:
            self.logger.info('Main|%s', handler.title)
            self.slides.show_tutorial(handler)
            while self.loop(handler):
                pass
        self.logger.info('Main|End')
        self.slides.show_end()
        self.logger.info('Main|Term')

class Preferences:
    def __init__(self, dictionary=None):
        if not dictionary:
            dictionary = {k: v[0] for k, v in strings.preferences.items()}
        self.debug = dictionary['Geliştirici Modu'] == 'Açık'
        self.inch = float(dictionary['Ekran Boyutu (inç)'])
        self.resolution = tuple(map(int, dictionary['Ekran Çözünürlüğü'].split('x')))
        self.full_screen = dictionary['Tam Ekran'] == 'Açık'
        self.width = self.inch * 2.54 * \
            self.resolution[0] / (self.resolution[0] ** 2 +
                                  self.resolution[1] ** 2) ** .5

if __name__ == "__main__":
    colorblind.ensure_files()
    if len(sys.argv) == 1:
        App(Preferences()).run()
    elif sys.argv[1] == 'launcher':
        App(Preferences(dialog.show_launcher())).run()
