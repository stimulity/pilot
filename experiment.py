from enum import Enum
import numpy as np
from psychopy import data
import strings

class TrialType(Enum):
    TRAINING = 0
    VALIDATION = 1
    TESTING = 2

class HandlerList(list):
    def __init__(self, path, logger):
        self.path = path + '.csv'
        self.first_write = True

        self.first_load = np.random.randint(2)
        self.first_space = np.random.randint(2)

        self.breaks = [new_breaks() for _ in range(4)]

        logger.info('Handler|%s', {'first_space': self.first_space,
                                   'first_load': self.first_load,
                                   'breaks': self.breaks})

        for level in range(2):
            magnitude = level ^ self.first_space

            trialList = [{
                          'cognitive_load': 0,
                          'magnitude': magnitude,
                          'quantity': q} for q in [0, 6]]
            trial = data.TrialHandler(trialList, 2, 'sequential')
            trial.type = TrialType.TRAINING
            trial.level = level
            trial.title = strings.get_title(0)
            trial.description = strings.get_description(magnitude, 0)
            self.append(trial)

            def new_validation(cognitive_load, magnitude):
                trialList = [{
                              'cognitive_load': cognitive_load,
                              'magnitude': magnitude,
                              'quantity': q} for q in [0, 6]]
                trial = data.TrialHandler(trialList, 2, 'fullRandom')
                trial.type = TrialType.VALIDATION
                trial.level = level
                trial.number = cognitive_load
                trial.title = strings.get_title(1, cognitive_load)
                trial.description = strings.get_description(magnitude, 1, cognitive_load)
                trial.lives = 1
                trial.new = lambda: new_validation(cognitive_load, magnitude)
                return trial
            self.append(new_validation(0, magnitude))
            if level == 0:
                self.append(new_validation(1, magnitude))

            for block in range(2):
                cognitive_load = block ^ self.first_load
                trialList = [{
                              'cognitive_load': cognitive_load,
                              'magnitude': magnitude,
                              'quantity': q} for q in range(7)]
                trial = data.TrialHandler(trialList, 1 + 8)
                trial.type = TrialType.TESTING
                trial.level = level
                trial.number = block
                trial.title = strings.get_title(2, cognitive_load)
                trial.description = strings.get_description(magnitude, 2, cognitive_load)
                trial.breaks = self.breaks[level * 2 + block]
                self.append(trial)

    def save(self, trial):
        if trial.type == TrialType.TESTING:
            if self.first_write:
                trial.saveAsWideText(self.path, matrixOnly=False, encoding='utf-8')
                self.first_write = False
            else:
                trial.saveAsWideText(self.path, matrixOnly=True, encoding='utf-8')

def new_breaks():
    LOW = 5
    HIGH = 15
    N = (1 + 8) * 7
    while True:
        breaks = np.sort(np.random.choice(N, 5, replace=False))
        diffs = np.diff(np.concatenate(([0], breaks, [N])))
        if (diffs >= LOW).all() and (diffs < HIGH).all():
            return breaks.tolist()
