from PyQt5.QtCore import Qt
from psychopy import gui, visual
import colorblind
import strings

class FormDialog(gui.Dlg):
    def __init__(self, dictionary, labels):
        super(FormDialog, self).__init__(title='Form')
        self.dictionary = dictionary.copy()
        self.labels = labels

        for key, label in self.labels.items():
            if key.startswith('__text__'):
                self.addText(label)
            elif type(self.dictionary[key]) in [list, tuple]:
                self.addField(label, choices=self.dictionary[key])
            else:
                self.addField(self.labels[key], self.dictionary[key])

        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.show()

    def show(self):
        ok_data = self.exec_()
        if ok_data:
            for i, key in enumerate(self.dictionary.keys()):
                self.dictionary[key] = self.data[i]

def failure_exit(reason):
    gui.criticalDlg('Hata', reason)
    raise SystemExit

def show_user_data_dialog(logger):
    dialog1 = FormDialog(strings.form_data_1, strings.form_label_1)
    if not dialog1.OK or (dialog1.dictionary['age'] == strings.invalid_age or
                          dialog1.dictionary['requirement0'] == strings.invalid_requirement[0] or
                          dialog1.dictionary['requirement1'] == strings.invalid_requirement[1] or
                          dialog1.dictionary['requirement2'] == strings.invalid_requirement[2] or
                          dialog1.dictionary['language'] in strings.invalid_languages):
        failure_exit(strings.failed_form)
    logger.info('Form 1|%s', dialog1.dictionary)
    dialog2 = FormDialog(strings.form_data_2, strings.form_label_2)
    if not dialog2.OK or (list(dialog2.dictionary.values()).count('Sol') * 3
                          + list(dialog2.dictionary.values()).count('Her ikisi de') * 2
                          + list(dialog2.dictionary.values()).count('Sağ') * 1 > 17):
        failure_exit(strings.failed_handedness)
    logger.info('Form 2|%s', dialog2.dictionary)

def show_colorblind_test_dialog(logger):
    result = colorblind.show_dialog(logger)
    if not result:
        failure_exit(strings.failed_colorblind)

def show_all(prefs, logger):
    win = visual.Window(size=prefs.resolution, allowGUI=False)
    win.mouseVisible = True
    show_user_data_dialog(logger)
    show_colorblind_test_dialog(logger)
    win.close()

def show_launcher():
    dialog = gui.DlgFromDict(strings.preferences.copy(), title='Pilot')
    if not dialog.OK:
        raise SystemExit
    return dialog.dictionary
